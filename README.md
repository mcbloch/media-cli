# Media cli

Very much in a broken state. I'm rewriting the ui in a tui because I wanted it to be more graphical.

A cli script to manage your media. Currently development solely for video's but can be used for any file.


## Features
- Get an easy list of your media files
- Select an entry to open/play it.
- See if and when you last viewed a file. (only works when you open files via this cli script)

## Install instructions

You only need rust installed.

1. Clone the repo. (for ex. in ~/.local/src)

        git clone git@gitlab.com:mcbloch/media-cli.git ~/.local/src

2. Install the application
 
        cargo install --path ~/.local/src/media-cli



## Todo's

- Support multiple file locations
- Filter media using a fuzzy search
- Support series
  - Group all episodes together under the name of the series
  - Group all episodes of a season together
  - Remember the last played episode, mark it. 
  - have it play by current of next serie when selecting the name of the serie
        maybe have quick option. Continue or next, with a timer how long you saw the current one
- get metadata of files (duration, resolution, ...)