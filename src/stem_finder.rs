/*
def findstem(arr):

    # Determine size of the array
    n = len(arr)

    # Take first word from array
    # as reference
    s = arr[0]
    l = len(s)

    res = ""

    for i in range( l) :
        for j in range( i + 1, l + 1) :

            # generating all possible substrings
            # of our reference string arr[0] i.e s
            stem = s[i:j]
            k = 1
            for k in range(1, n):

                # Check if the generated stem is
                # common to all words
                if stem not in arr[k]:
                    break

            # If current substring is present in
            # all strings and its length is greater
            # than current result
            if (k + 1 == n and len(res) < len(stem)):
                res = stem

    return res
*/

use ordered_float::OrderedFloat;
use std::cmp;
use std::collections::HashSet;

#[derive(Debug, Clone)]
pub struct StringMatch {
    pub value: String,
    matches: usize,
    match_list: Vec<usize>,
    score: f64,
}
/*
program to find the stem
# of given list of words

# function to find the stem (longest
# common substring) from the string array
    */
pub fn findstem(mut arr: Vec<String>) -> Vec<StringMatch> {
    let n = arr.len();
    // will contain the id's of the already selected items
    let mut blacklist: HashSet<usize> = HashSet::new();
    let mut global_stringmatches: Vec<StringMatch> = Vec::new();

    for u in 0..arr.len() {
        if blacklist.contains(&u) {
            // When the entry is already in the selection of another, don't search matches
            continue;
        }
        // Take first string from array
        let s = &arr[u];
        let l = s.len();

        let mut string_matches: Vec<StringMatch> = Vec::new();
        let mut matches = 0;
        let mut res: String = String::new();

        println!("Checking {:?}", s);

        for i in 0..l {
            for j in i + 1..l + 1 {
                // Generate all possible substrings of
                // our reference string arr[0] i.e s
                let stem = s[i..j].to_string();
                let mut local_matches = 0;
                let mut match_list: Vec<usize> = Vec::new();

                // println!("Trying {}", stem);
                for k in 1..n {
                    // Check if the generated stem is common
                    // to all words
                    if arr[k].contains(&stem) && !blacklist.contains(&k) {
                        local_matches += 1;
                        match_list.push(k)
                    }
                }
                // If current substring is present in
                // all strings and its length is greater
                // then current result
                if (res.len() < stem.len() || local_matches > matches)
                    && local_matches > 0
                    && stem.len() > 2
                {
                    let mut score = 0.;

                    // Give a bonus when bounded by non-alphanumeric characters
                    score += 1.0;
                    if j == s.len() || !s.chars().nth(j).unwrap().is_alphanumeric() {
                        score -= 0.5
                    }
                    if i == 0 || !s.chars().nth(i - 1).unwrap().is_alphanumeric() {
                        score -= 0.5
                    }

                    score += ((i as f64) / (l as f64)) * 2.0; // prefer close to the start
                    score += (1.0 - ((local_matches as f64) / (arr.len() as f64))) * 3.0; // prefer most matches
                    score += (1.0 - ((stem.len() as f64) / (s.len() as f64))) * 1.0; // prefer longer stem
                                                                                     // weg
                                                                                     // println!(" Succes, '{}' is found in all other strings and is the longest", stem);
                    string_matches.push(StringMatch {
                        value: stem.to_string(),
                        matches: local_matches,
                        match_list,
                        score,
                    });
                }
            }
        }
        string_matches.sort_unstable_by_key(|entry| {
            (
                OrderedFloat(entry.score)
                // cmp::Reverse(entry.value.len()),
                // entry.matches,
            )
        });
        for stringmatch in string_matches.get(0) {
            // weg
            println!(
                "Stem: '{}' with {} matches, score: {:?}",
                stringmatch.value, stringmatch.matches, stringmatch.score
            );
            global_stringmatches.push(stringmatch.clone());
            for wmatch in &stringmatch.match_list {
                blacklist.insert(*wmatch);
                println!("   {}", arr[*wmatch]); // weg
            }
        }
        println!("------------------")
    }

    for stringmatch in &global_stringmatches {
        println!(
            "Stem: '{}' with {} matches, score: {:?}",
            stringmatch.value, stringmatch.matches, stringmatch.score
        );
        for wmatch in &stringmatch.match_list {
            println!("  .. {}", arr[*wmatch]);
        }
    }

    return global_stringmatches;
}

fn main() {
    let arr: Vec<String> = [
        "grace".to_string(),
        "graceful".to_string(),
        "disgraceful".to_string(),
        "gracefully".to_string(),
    ]
    .to_vec();
    let stems = findstem(arr);
    // println!("Longest substring in all strings: {}", stems);
}
