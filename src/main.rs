extern crate open;
extern crate serde_derive;
#[macro_use]
extern crate simple_error;
#[macro_use]
extern crate text_io;
extern crate toml;
extern crate xdg;

use mpsc::Receiver;
use std::cmp;
use std::collections::HashMap;
use std::collections::HashSet;
use std::convert::TryInto;
use std::error::Error;
use std::fs;
use std::fs::OpenOptions;
use std::fs::{symlink_metadata, File};
use std::io;
use std::io::BufRead;
use std::ops::Index;
use std::path::Path;
use std::path::PathBuf;
use std::process::exit;
use std::result::Result;
use std::sync::mpsc;
use std::sync::mpsc::channel;
use std::sync::Arc;
use std::sync::RwLock;
use std::time::Instant;
use std::{thread, time};

use chrono::{Date, DateTime, Utc};
use dialoguer::{Input, Select};
use glob::glob;
use prompts::autocomplete::AutocompletePrompt;
use prompts::Prompt;
use regex::Regex;
use regex::RegexBuilder;
use serde_derive::{Deserialize, Serialize};
use termion::event;
use termion::input::{EventsAndRaw, TermRead};

use data_manager::my_date_format;

use crate::ui::{init_term, render, Term};

type BoxResult<T> = Result<T, Box<dyn Error>>;

mod data_manager;

mod stem_finder;
mod ui;

#[derive(Serialize, Deserialize)]
struct Config {
    media_dir: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct WatchLog {
    media_name: String,
    #[serde(with = "my_date_format")]
    timestamp: DateTime<Utc>,
    duration: u64,
}

#[derive(Debug)]
struct MediaEntry {
    media_path: PathBuf,
    media_name: String,
    year: String,
    episode: Option<SerieEpisode>,
    last_seen: Date<Utc>,
    seen_count: usize,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct SerieData {
    episode_list: Vec<String>,
}

#[derive(Debug)]
struct SerieEpisode {
    season: String,
    episode: String,
    name: String,
}

// Usage: map!{ 1 => "one", 2 => "two" };
macro_rules! map (
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

// use regex::Regex;

fn read_files(media_dir: String) -> Vec<PathBuf> {
    let mut media_files: Vec<PathBuf> = Vec::new();
    for entry in glob(&format!("{}/**/*.mp4", media_dir))
        .unwrap()
        .chain(glob(&format!("{}/**/*.mkv", media_dir)).unwrap())
    {
        match entry {
            Ok(path) => {
                media_files.push(path);
            }
            Err(e) => eprintln!("ERROR: {}", e),
        }
    }
    media_files
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

fn read_files_from_file(file: String) -> Vec<String> {
    let mut media_files: Vec<String> = Vec::new();

    // File hosts must exist in current path before this produces output
    if let Ok(lines) = read_lines("./hosts") {
        // Consumes the iterator, returns an (Optional) String
        for line in lines {
            if let Ok(ip) = line {
                println!("{}", ip);
                media_files.push(ip)
            }
        }
    }
    media_files
}

fn save_watchlog_data_to_file(
    data_path: PathBuf,
    data: WatchLog,
    write_headers: bool,
) -> Result<(), Box<dyn Error>> {
    match OpenOptions::new().write(true).append(true).open(data_path) {
        Err(err) => bail!("Could not open data file to append: {:?}", err),
        Ok(file) => {
            let mut wtr = csv::WriterBuilder::new()
                .has_headers(false)
                .from_writer(file);

            // We still need to write headers manually.
            println!("Writing to csv");
            if write_headers {
                wtr.write_record(&["media_name", "timestamp", "duration"])?;
            }

            wtr.serialize(data)?;

            wtr.flush()?;
            Ok(())
        }
    }
}

fn save_to_watchlog(watchlog: WatchLog) -> Result<(), Box<dyn Error>> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();
    match xdg_dirs.find_data_file("watchlog.csv") {
        None => match xdg_dirs.place_data_file("watchlog.csv") {
            Err(_) => {
                bail!("Could not create the correct directories");
            }
            Ok(data_path) => match File::create(&data_path) {
                Err(err) => {
                    bail!("Could not create the data file, error: {}", err);
                }
                Ok(data_file) => {
                    println!("Created data file at: {:?}", data_file);
                    return Ok(save_watchlog_data_to_file(data_path, watchlog, true)?);
                }
            },
        },
        Some(data_path) => {
            return Ok(save_watchlog_data_to_file(data_path, watchlog, false)?);
        }
    }
}

fn load_watchlogs() -> Option<HashMap<String, Vec<WatchLog>>> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();
    match xdg_dirs.find_data_file("watchlog.csv") {
        None => {
            return None;
        }
        Some(data_path) => match File::open(data_path) {
            Err(err) => {
                println!("Could not open the data file");
                return None;
            }
            Ok(data_file) => {
                let mut rdr = csv::Reader::from_reader(data_file);
                let mut grouped_logs: HashMap<String, Vec<WatchLog>> = HashMap::new();

                for log_res in rdr.deserialize() {
                    match log_res {
                        Err(err) => println!("Invalid data entry: {}", err),
                        Ok(log) => {
                            let log: WatchLog = log;
                            let media_name = log.media_name.to_string();
                            match grouped_logs.get_mut(&media_name) {
                                Some(log_group) => log_group.push(log),
                                None => {
                                    let mut log_group: Vec<WatchLog> = Vec::new();
                                    log_group.push(log);
                                    grouped_logs.insert(media_name.to_string(), log_group);
                                }
                            }
                        }
                    }
                }
                return Some(grouped_logs);
            }
        },
    }
}

fn open_file(file: &PathBuf) {
    match open::that(file) {
        Ok(exit_status) => {
            if exit_status.success() {
                // println!("Look at your media viewer!");
            } else if let Some(code) = exit_status.code() {
                println!("Command returned non-zero exit status {}!", code);
            } else {
                println!("Command returned with unknown exit status!");
            }
        }
        Err(why) => println!("Failure to execute command: {}", why),
    }
}

fn get_serie(filename: &String) -> Option<SerieEpisode> {
    let re = Regex::new(r"(S[0-9]{1,2})").unwrap();
    match re.captures(filename.as_str()) {
        None => None,
        Some(cap) => {
            if cap.len() == 0 {
                None
            } else {
                let season: &str = &cap[1];
                return Some(SerieEpisode {
                    season: season.to_owned(),
                    episode: "".to_owned(),
                    name: "".to_owned(),
                });
            }
        }
    }
}

fn clean_filename(path: &PathBuf) -> (String, Option<String>, Option<SerieEpisode>) {
    match path.file_stem() {
        // file_name()
        None => ("Could not parse filename".to_string(), None, None),
        Some(filename_osstr) => {
            match filename_osstr.to_str() {
                Some(filestr) => {
                    // let file_stem = filestr.file_stem()

                    // let re = Regex::new(r"(.*)\.(mkv|mp4)").unwrap();
                    // let after = re.replace(&filestr, "$1"); // Remove trailing extension

                    // Replace dots by spaces
                    let cleaned_filename = filestr.replace(".", " ");
                    let serie = get_serie(&cleaned_filename);
                    let re = Regex::new(r" ([0-9]{4}) ?").unwrap();
                    return match re.captures(cleaned_filename.as_str()) {
                        None => (cleaned_filename, None, serie),
                        Some(cap) => {
                            let year: &str = &cap[1];
                            // println!("Year detected: {}", year.as_str().to_string());
                            (cleaned_filename.to_string(), Some(year.to_string()), serie)
                        }
                    };
                }
                _ => ("Could not parse filename".to_string(), None, None),
            }
        }
    }
}

// fn config_default_options() -> {
//
// }

fn config_enter_custom_option() -> String {
    // TODO Add checks that folder exists etc...
    Input::<String>::new()
        .with_prompt("Please enter a media path")
        .interact()
        .expect("could not ask for input")
}

// fn config_remove_options() -> {
//
// }

fn set_config() {
    let media_dir = config_enter_custom_option();
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();

    let config_path = xdg_dirs
        .place_config_file("config.toml")
        .expect("cannot create configuration directory");
    // let mut config_file_res = File::create(&config_path);
    // match config_file_res {
    //     Err(e) => println!("Could not create config file."),
    //     Ok(mut config_file) => {
    let config = Config { media_dir };
    let toml_string = toml::to_string(&config).expect("Could not encode TOML value");
    // println!("{}", toml_string);
    fs::write(&config_path, toml_string).expect("Could not write to config file!");

    // term.write_line(format!("Media directory set to: {}", media_dir).as_str());
    let _ = println!(
        "You can later edit this value in the following location {:?}",
        config_path
    );
}

fn get_config() -> Config {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();
    match xdg_dirs.find_config_file("config.toml") {
        None => {
            // application config not present
            set_config();
            return get_config();
        }
        Some(config_path) => {
            match fs::read_to_string(config_path) {
                Err(_why) => {
                    // Could not read config file
                    set_config();
                    return get_config();
                }
                Ok(configfile_content) => {
                    let config: Config = toml::from_str(&configfile_content).unwrap();
                    return config;
                }
            }
        }
    }
}

// TODO load serie info

fn save_serie_info(series: HashMap<String, SerieData>) {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();
    let data_path = xdg_dirs
        .place_data_file("series.toml")
        .expect("cannot create data directory");
    let toml_string = toml::to_string(&series).expect("Could not encode TOML value");
    fs::write(&data_path, toml_string).expect("Could not write to data file!");
}

fn load_serie_info() -> HashMap<String, SerieData> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("media_cli").unwrap();
    match xdg_dirs.find_data_file("series.toml") {
        None => {
            save_serie_info(HashMap::new());
            load_serie_info()
        }
        Some(data_path) => {
            match fs::read_to_string(data_path) {
                Err(why) => {
                    // Could not read file
                    println!("Error reading series.toml data file.");
                    println!("{:?}", why);
                    HashMap::new()
                }
                Ok(datafile_content) => {
                    let data: HashMap<String, SerieData> = toml::from_str(&datafile_content)
                        .expect("series.toml not a valid toml format");
                    data
                }
            }
        }
    }
}

pub struct UiState {
    input: String,
    main: Vec<String>,
    logs: Vec<String>,
}

struct TuiConfig {
    exit_key: termion::event::Key,
}

fn main() -> Result<(), std::io::Error> {
    let tuiconfig = TuiConfig {
        exit_key: termion::event::Key::Esc,
    };

    let mut term = init_term().expect("Could not initialize terminal.");

    let mut state_lock_arc = Arc::new(RwLock::new(UiState {
        input: String::new(),
        main: Vec::new(),
        logs: Vec::new(),
    }));

    let config: Config = get_config();

    // Create a list of all files
    let media_files: Vec<PathBuf> = read_files(config.media_dir);

    let mut media_entries: Vec<MediaEntry> = Vec::new();
    let watchlogs_per_media_entry = load_watchlogs().unwrap_or(HashMap::new());

    // Clean the found files in a readable format and add them to the prompt
    for path in &media_files {
        let (cleaned_filename, year, episode) = clean_filename(path);
        let mut media_entry = MediaEntry {
            media_path: path.to_path_buf(),
            media_name: cleaned_filename,
            last_seen: chrono::MIN_DATE,
            seen_count: 0,
            year: year.unwrap_or(String::new()),
            episode,
        };
        match watchlogs_per_media_entry.get(&media_entry.media_name) {
            None => {}
            Some(logs) => {
                media_entry.seen_count = logs.len();
                if media_entry.seen_count > 0 {
                    media_entry.last_seen = logs.into_iter().fold(chrono::MIN_DATE, |acc, x| {
                        return if x.timestamp.date() > acc {
                            x.timestamp.date()
                        } else {
                            acc
                        };
                    });
                }
            }
        }
        media_entries.push(media_entry);
    }

    // Create groups of media

    // sort the entries list for sensible viewing
    media_entries.sort_unstable_by_key(|entry| {
        (
            cmp::Reverse(entry.last_seen),
            entry.media_name.to_string(),
            entry.year.to_string(),
        )
    });

    let media_sensible_names: Vec<String> = (&media_entries)
        .into_iter()
        .map(|x| x.media_name.to_owned())
        .collect();

    let (tx, rx) = mpsc::channel();
    let _input_handle = {
        let tx = tx.clone();
        thread::spawn(move || {
            let stdin = io::stdin();
            for evt in stdin.keys() {
                match evt {
                    Ok(key) => {
                        if let Err(_) = tx.send(key) {
                            // Event::Input(key)
                            return;
                        }
                        if key == tuiconfig.exit_key {
                            exit(0)
                        }
                    }
                    Err(_) => {}
                }
            }
        })
    };

    let _action_handle = {
        let state_lock = Arc::clone(&state_lock_arc);
        thread::spawn(move || {
            other(media_entries, media_sensible_names, rx, state_lock);
            return 200;
        });
    };

    // Key presses change state
    // Based on this state certain action can be taken.
    let state_lock = Arc::clone(&state_lock_arc);
    loop {
        // term.clear();
        // contents of the new frame
        {
            let state_read = state_lock.read().unwrap();
            term.draw(|f| render(f, &state_read));
        }
        let ten_millis = time::Duration::from_millis(10);
        thread::sleep(ten_millis);
    }
}

// Returns when enter is pressed
fn handle_normal_input(
    rx: &Receiver<termion::event::Key>,
    everything_lock: &RwLock<UiState>,
) -> String {
    // event handling
    let mut key = rx.recv().expect("Channel error :think:");
    while key != termion::event::Key::Char('\n') {
        match key {
            termion::event::Key::Backspace => {
                let mut everything = everything_lock.write().unwrap();
                everything.input.pop();
            }
            termion::event::Key::Char(c) => {
                let mut everything = everything_lock.write().unwrap();
                everything.input.push(c)
            }
            _ => {}
        }
        key = rx.recv().expect("Channel error :think:");
    }
    let input = {
        let state_read = everything_lock.read().unwrap();
        state_read.input.to_owned()
    };
    {
        let mut everything = everything_lock.write().unwrap();
        everything.input.clear();
    }
    return input;
}

fn other(
    media_entries: Vec<MediaEntry>,
    media_sensible_names: Vec<String>,
    rx: Receiver<termion::event::Key>,
    everything_lock: Arc<RwLock<UiState>>,
) -> Result<(), std::io::Error> {
    // TODO
    // Eerste file zonder match pakken. Suggestie geven over wat de serie naam kan zijn.
    // Deze laten aanpassen door de gebruiker.
    // Alle matchende episodes zoeken.
    // De serie met matchende episodes opslaan.

    // will contain the id's of the already selected items
    let mut series: HashMap<String, SerieData> = load_serie_info();

    let mut blacklist: HashSet<usize> = HashSet::new();
    // TODO very inefficient, consider saving the blacklist to disk
    for (serie_naam, serie_data) in &series {
        for episode in &serie_data.episode_list {
            for i in 0..media_sensible_names.len() {
                let name = &media_sensible_names[i];
            }
        }
    }

    // Find the first file that is yet open
    let n = media_sensible_names.len();

    let mut is_done = String::from("");

    while is_done != "Done" {
        for u in 0..n {
            if blacklist.contains(&u) {
                // When the entry is already in the selection of another, don't search matches
                continue;
            }
            let s = &media_sensible_names[u];

            {
                let mut state_write = everything_lock.write().unwrap();
                let _ = state_write.logs.push(String::from("Non-matched series: "));
                let _ = state_write
                    .logs
                    .push(String::from(&format!("   - {:?}", s)));
            }
            break;
        }

        {
            let mut state_write = everything_lock.write().unwrap();
            let _ = state_write.logs.push(String::from(
                "Give a serie name so we can group episodes for you.",
            ));
        }
        // contents of the new frame

        let input = handle_normal_input(&rx, &everything_lock);
        let re_convert_serie_name = Regex::new(r"[^a-zA-Z0-9]").unwrap();
        let converted_serie_name = re_convert_serie_name.replace_all(&input, ".?");

        {
            let mut state_write = everything_lock.write().unwrap();
            state_write.logs.push(String::from(&format!(
                "Fuzzy matcher: {:?}",
                &converted_serie_name
            )));
        }

        let re_serie_name = RegexBuilder::new(&converted_serie_name)
            .case_insensitive(true)
            .build()
            .expect("Invalid Regex");

        let mut episode_matches: Vec<usize> = Vec::new();

        for k in 0..n {
            let filename = &media_sensible_names[k];

            if !blacklist.contains(&k) && re_serie_name.is_match(filename) {
                {
                    let mut state_write = everything_lock.write().unwrap();
                    state_write
                        .main
                        .push(String::from(&format!("  Match: {:?}", filename)));
                }
                episode_matches.push(k);
            } else {
                // println!("No match: {:?}", filename);
            }
        }

        {
            let mut state_write = everything_lock.write().unwrap();
            state_write
                .logs
                .push(String::from(&format!(" *Name: {}", input)));
            state_write.logs.push(String::from(&format!(
                " *Found {} episodes.",
                episode_matches.len()
            )));
            state_write
                .logs
                .push(String::from(&format!("Is this correct? (y/n)")));
        }

        let check_prompt: String = handle_normal_input(&rx, &everything_lock);

        match check_prompt.as_str() {
            "y" => {
                let mut episode_matches_names: Vec<String> = Vec::new();
                for i in episode_matches {
                    let filename = &media_sensible_names[i];
                    blacklist.insert(i);
                    episode_matches_names.push(filename.clone());
                }
                series.insert(
                    input.to_owned(),
                    SerieData {
                        episode_list: episode_matches_names,
                    },
                );
            }
            _ => {
                let mut state_write = everything_lock.write().unwrap();
                state_write
                    .logs
                    .push(String::from(&format!("Let's try this again then.")));
            }
        }

        if blacklist.len() == media_sensible_names.len() {
            let mut state_write = everything_lock.write().unwrap();

            state_write.logs.push(String::from(&format!(
                "No orphan episodes left. Exiting..."
            )));
            continue;
        } else {
            {
                let mut state_write = everything_lock.write().unwrap();
                let _ = state_write.logs.push(String::from(&format!(
                    "Type 'Done' if you are ready, anything else to continue."
                )));
            }
            is_done = handle_normal_input(&rx, &everything_lock);
        }
    }

    save_serie_info(series);

    // -------------------------------------------------------------------

    return Ok(());

    let mut select = Select::new();

    // select.paged(true);
    select.with_prompt("Please select your media file");

    // Add the entries to the choicebox
    for entry in &media_entries {
        let prefix: String;
        if entry.seen_count == 0 {
            prefix = "not seen".to_string();
        } else {
            prefix = format!("{}", entry.last_seen.format("%F"));
        }

        let mut year_prefix: String = String::new();
        if entry.year != "" {
            year_prefix = format!("({})", entry.year)
        }

        let mut serie_string = String::new();

        match &entry.episode {
            Some(serieEpisode) => {
                serie_string = format!(" - [{}:{}]", serieEpisode.season, serieEpisode.episode);
            }
            _ => {}
        }

        select.item(format!(
            "[{:10}] {} {} {}",
            prefix, year_prefix, entry.media_name, serie_string
        ));
    }

    select.default(0);
    let res = select.interact_opt();

    match res {
        Err(why) => panic!("{:?}", why),
        Ok(choice) => match choice {
            None => {
                let _ = println!("Nothing selected");
            }
            Some(index) => match media_entries.get(index) {
                None => {
                    let _ = println!("Selected option not found in list.");
                }
                Some(media_entry) => {
                    let _ = println!("Option chosen: {:?}", media_entry.media_name);
                    let mut watchlog = WatchLog {
                        media_name: format!("{}", media_entry.media_name),
                        timestamp: Utc::now(),
                        duration: 0,
                    };
                    let start = Instant::now();
                    open_file(&media_entry.media_path);
                    let elapsed_time = start.elapsed().as_secs();
                    println!("{}s time elapsed", elapsed_time);
                    if elapsed_time > 1 {
                        watchlog.duration = elapsed_time;
                        match save_to_watchlog(watchlog) {
                            Err(err) => {
                                let _ = println!("Error occured saving the log: {}", err);
                            }
                            Ok(()) => {}
                        }
                    }
                }
            },
        },
    }
    Ok(())
}
