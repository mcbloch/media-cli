use std::io;
use std::io::Stdout;

use termion::raw::{IntoRawMode, RawTerminal};
use termion::screen::AlternateScreen;
use tui::backend::TermionBackend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::Modifier;
use tui::style::{Color, Style};
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, Paragraph, Wrap};
use tui::{Frame, Terminal};

use crate::UiState;

pub(crate) type Term = Terminal<TermionBackend<AlternateScreen<RawTerminal<Stdout>>>>;

pub fn init_term() -> Result<Term, io::Error> {
    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    terminal.hide_cursor()?;
    let _ = terminal.clear();
    return Ok(terminal);
}

pub fn render(
    f: &mut Frame<TermionBackend<AlternateScreen<RawTerminal<Stdout>>>>,
    everything: &UiState,
) {
    let size = f.size();

    // Background
    let block = Block::default().style(Style::default().bg(Color::White).fg(Color::Black));
    f.render_widget(block, size);

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(2)
        .constraints(
            [
                Constraint::Percentage(10),
                Constraint::Percentage(70),
                Constraint::Percentage(20),
            ]
            .as_ref(),
        )
        .split(size);

    let create_block = |title| {
        Block::default()
            .borders(Borders::ALL)
            .style(Style::default().bg(Color::White).fg(Color::Black))
            .title(Span::styled(
                title,
                Style::default().add_modifier(Modifier::BOLD),
            ))
    };

    let input_widget = Paragraph::new(vec![Spans::from(vec![Span::raw(&everything.input)])])
        .block(create_block("Input"));
    f.render_widget(input_widget, chunks[0]);

    let paragraph = Paragraph::new("")
        .style(Style::default().bg(Color::White).fg(Color::Black))
        .block(create_block("Main"))
        .alignment(Alignment::Center)
        .wrap(Wrap::default());
    f.render_widget(paragraph, chunks[1]);

    let mut text = vec![
        // Spans::from(vec![
        //     Span::raw("First"),
        //     Span::styled("line", Style::default().add_modifier(Modifier::ITALIC)),
        //     Span::raw("."),
        // ]),
        // Spans::from(Span::styled("Second line", Style::default().Color::Red))),
    ];
    for line in &everything.logs {
        text.push(Spans::from(vec![Span::raw(line)]))
    }

    let mut wrap = Wrap::default();
    wrap.scroll_callback = Some(Box::new(|text_area, lines| {
        let len = lines.len() as u16;
        (len.saturating_sub(text_area.height), 0)
    }));

    let p_block = create_block("Log");
    let log_widget = Paragraph::new(text).block(p_block).wrap(wrap);
    f.render_widget(log_widget, chunks[2]);
}
